package model;

public class Ticket {
    private int id;
    private int flightId;
    private String userLogin;

    public Ticket(int id, int flightId, String userLogin) {
        this.id = id;
        this.flightId = flightId;
        this.userLogin = userLogin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", flightId=" + flightId +
                ", userLogin='" + userLogin + '\'' +
                '}';
    }
}
