package servlets;

import dao.FlightDAO;
import model.Airport;
import model.Flight;
import service.AirportService;
import service.FlightService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
@WebServlet("/EditFlightServlet")
public class EditFlightServlet extends HttpServlet {
    @EJB
    FlightService flightService;
    @EJB
    AirportService airportService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id =req.getParameter("which");
        try {
            List<Airport> airports = airportService.allAirports();
            req.setAttribute("airportList",airports);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
           Flight f = flightService.searchFlight(Integer.parseInt(id));
           req.setAttribute("flight",f);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        req.setAttribute("which",id);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("/editFlight.jsp");
        requestDispatcher.forward(req,resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String arrivalDate = req.getParameter("arrivalDate");
        String departureDate = req.getParameter("departureDate");
        String airportFrom = req.getParameter("airportFrom");
        String airportTo = req.getParameter("airportTo");
        String price = req.getParameter("price");
        String seats = req.getParameter("seats");
        String type = req.getParameter("type");
        flightService.editFlight(id,arrivalDate,departureDate,airportFrom,airportTo,price,seats,type);
        resp.sendRedirect("index.jsp");




    }

}

