package servlets;

import service.TicketService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addTicket")
public class AddTicketServlet extends HttpServlet {
    @EJB
    TicketService ticketService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("addTicket.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String flightId = req.getParameter("flightIdAdd");
        String userLog = req.getParameter("userLoginAdd");
        boolean exist = false;
        ticketService.addTicket(0+"",flightId,userLog,exist);
        resp.sendRedirect("index.jsp");




    }
}
