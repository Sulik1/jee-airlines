package servlets;

import dao.TicketDAO;
import service.TicketService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/removeTicket")
public class RemoveTicketServlet extends HttpServlet {
    @EJB
    TicketService ticketService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer which = Integer.parseInt(req.getParameter("which"));
        try {
            ticketService.removeTicket(which);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("index.jsp");

    }
}
