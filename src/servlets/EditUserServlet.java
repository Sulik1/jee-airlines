package servlets;

import service.UserService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/editUser")
public class EditUserServlet extends HttpServlet {
    @EJB
    UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String loginEdit = req.getParameter("which");
        req.setAttribute("which",loginEdit);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("/editUser.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String password = req.getParameter("passwordEdit");
        String name = req.getParameter("nameEdit");
        String surname = req.getParameter("surnameEdit");
        String city = req.getParameter("cityEdit");
        String date = req.getParameter("dateEdit");
        String loginEdit = req.getParameter("which");
        boolean exist = false;
        try {
            userService.editUSer(password,name,surname,loginEdit,city,date,exist);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("index.jsp");
    }
}
