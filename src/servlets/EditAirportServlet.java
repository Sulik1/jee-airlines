package servlets;

import service.AirportService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/editAirport")
public class EditAirportServlet extends HttpServlet {
    @EJB
    AirportService airportService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String code = req.getParameter("which");
        req.setAttribute("which",code);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("/editAirport.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String city = req.getParameter("cityEdit");
        String country = req.getParameter("countryEdit");
        String code = req.getParameter("which");
        boolean exsist = false;
        try {
            airportService.updateAirport(code,city,country,exsist);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("index.jsp");

    }
}
