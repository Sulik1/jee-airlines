package servlets;

import service.AirportService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addAirport")
public class AddAirportServlet extends HttpServlet {
    @EJB
    AirportService airportService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("addAirport.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String code = req.getParameter("codeAdd");
        String city = req.getParameter("cityAdd");
        String country = req.getParameter("countryAdd");
        airportService.addAirport(code,city,country);
        resp.sendRedirect("index.jsp");
    }
}
