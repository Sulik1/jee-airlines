package servlets;

import model.Airport;
import service.AirportService;
import service.FlightService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/addFlight")
public class AddFlightServlet extends HttpServlet {
    @EJB
    FlightService flightService;
    @EJB
    AirportService airportService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Airport> airports = new ArrayList<>();
        try {
            airports= airportService.allAirports();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("airportList",airports);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("addFlight.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String dateFrom = req.getParameter("dateDeAdd");
        String dateTo = req.getParameter("dateArAdd");
        String airportFrom = req.getParameter("airportFrom");
        String airportTo = req.getParameter("airportTo");
        String price = req.getParameter("price");
        String totalSeats = req.getParameter("totalSe");
        flightService.addFlight(dateFrom,dateTo,airportFrom,airportTo,price,totalSeats);
        resp.sendRedirect("index.jsp");
    }
}
