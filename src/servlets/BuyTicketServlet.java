package servlets;

import dao.TicketDAO;
import model.Ticket;
import service.TicketService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/buyTicket")
public class BuyTicketServlet extends HttpServlet {
    @EJB
    TicketService ticketService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String flightId = req.getParameter("id");
        String userLog =(String) session.getAttribute("login");
        ticketService.buyTicket(flightId,userLog);
        resp.sendRedirect("flightTable?bought="+flightId);


    }
}
