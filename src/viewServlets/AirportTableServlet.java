package viewServlets;

import model.Airport;
import service.AirportService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/airportTable")
public class AirportTableServlet extends HttpServlet {
    @EJB
    AirportService airportService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Airport> airports = new ArrayList<>();
        try {
            airports = airportService.allAirports();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("airportTable",airports);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("airportTable.jsp");
        requestDispatcher.forward(req,resp);
    }
}
