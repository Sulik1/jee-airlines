package viewServlets;

import model.Airport;
import model.Flight;
import service.AirportService;
import service.FlightService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/flightTable")
public class FlightTableServlet extends HttpServlet {
    @EJB
    FlightService flightService;
    @EJB
    AirportService airportService;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session1 =req.getSession();

        List<Flight> flights = new ArrayList<>();
        List<Airport> airports = new ArrayList<>();
        try {
            airports=airportService.allAirports();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            flights = flightService.allFlights();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (session1.getAttribute("login").equals("admin")){
            req.setAttribute("user","user");
        }
        req.setAttribute("airportTable",airports);
        req.setAttribute("flightTable",flights);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("flightTable.jsp");
        requestDispatcher.forward(req,resp);
    }
}
