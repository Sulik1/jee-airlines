package viewServlets;

import model.Ticket;
import service.TicketService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/ticketTable")
public class TicketTableServlet extends HttpServlet {
    @EJB
    TicketService ticketService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<Ticket> list = ticketService.allTickets();
            req.setAttribute("ticketList",list);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("ticketTable.jsp");
        requestDispatcher.forward(req,resp);
    }
}
