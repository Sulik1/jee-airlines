package viewServlets;

import model.Airport;
import model.Flight;
import service.AirportService;
import service.FlightService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/flightTableForUsers")
public class FlightTableForUserServlet extends HttpServlet {
    @EJB
    FlightService flightService;
    @EJB
    AirportService airportService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Flight> flights =new ArrayList<>();
        try {
            flights = flightService.allFlights();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("flightList",flights);
        resp.sendRedirect("flightTableForUsers.jsp");


    }
}
