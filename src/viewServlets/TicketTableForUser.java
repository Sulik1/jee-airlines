package viewServlets;

import model.Airport;
import model.Flight;
import model.Ticket;
import service.AirportService;
import service.FlightService;
import service.TicketService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/allMyTickets")
public class TicketTableForUser extends HttpServlet {
    @EJB
    AirportService airportService;
    @EJB
    TicketService ticketService;
    @EJB
    FlightService flightService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session12 = req.getSession();
        String login = (String) session12.getAttribute("login");
        List<Ticket> list = new ArrayList<>();
        List<Airport> list1 = new ArrayList<>();
        List<Flight> list2 = new ArrayList<>();

        try {
            list = ticketService.ticketForUser(login);
            list1 = airportService.allAirports();
            list2=flightService.allFlights();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("login",login);
        req.setAttribute("listTickets",list);
        req.setAttribute("listAirports",list1);
        req.setAttribute("listFlights",list2);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("allMyTickets.jsp");
        requestDispatcher.forward(req,resp);
    }
}
