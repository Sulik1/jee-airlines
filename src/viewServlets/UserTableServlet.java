package viewServlets;

import model.User;
import service.UserService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/userTable")
public class UserTableServlet extends HttpServlet {
    @EJB
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users = new ArrayList<>();
        try {
            users = userService.allUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("usersList",users);
        RequestDispatcher requestDispatcher =req.getRequestDispatcher("userTable.jsp");
        requestDispatcher.forward(req,resp);

    }
}
