package dao;

import connection.SqlConn;
import model.Ticket;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class TicketDAO {


    public  List<Ticket> getAllTickets() throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        Connection conn = SqlConn.getConnection();
        String sql = "select * from ticket";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            int id = rs.getInt("id_ticket");
            int idFlight = rs.getInt("id_flight");
            String login = rs.getString("login_user");
            tickets.add(new Ticket(id,idFlight,login));

        }
        return tickets;
    }

    public void addTicket(Ticket ticket) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "insert into ticket values(null,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,ticket.getFlightId());
        ps.setString(2,ticket.getUserLogin());
        ps.executeUpdate();

    }

    public Ticket searchTicketById(int id) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "select * from ticket where id_ticket=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()){
           int idF= rs.getInt("id_flight");
           String log = rs.getString("login_user");
            return new Ticket(id,idF,log);
        }else {
            return null;
        }
    }

    public void deleteTicket(int id) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "delete from ticket where id_ticket=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,id);
        ps.executeUpdate();
    }

    public  int howManyTicketOnThisFlight(int flightId) throws SQLException {
        int result=0;
        Connection conn = SqlConn.getConnection();
        String sql = "select count(*) as number from ticket where id_flight=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,flightId);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            int number = rs.getInt("number");
            result=number;
        }
        return result;
    }
    public  List<Ticket> allTicketForUser(String login) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        Connection conn = SqlConn.getConnection();
        String sql= "SELECT * from ticket where login_user=?";
        PreparedStatement ps =conn.prepareStatement(sql);
        ps.setString(1,login);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            int id = rs.getInt("id_ticket");
            int idFlight = rs.getInt("id_flight");
            tickets.add(new Ticket(id,idFlight,login));
        }
        return tickets;

    }




}
