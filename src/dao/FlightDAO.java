package dao;

import connection.SqlConn;
import model.Flight;

import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Stateless
public class FlightDAO {

    public  List<Flight> getAllFlights() throws SQLException {
        List<Flight> flights = new ArrayList<>();
        Connection conn = SqlConn.getConnection();
        String sql = "select * from flight";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            flights.add((getFlightFromData(rs)));

        }
return flights;
    }

    public  void addFlight(Flight flight) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "insert into flight values(null,?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        settAllButNoId(flight,ps);
        ps.executeUpdate();


    }

    public  Flight searchFlightById(int id) throws SQLException {
        Connection conn= SqlConn.getConnection();
        String sql = "Select * from flight where id_flight=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()){
            return getFlightFromData(rs);
        }else return null;

    }

    public  void deleteFlight(int id) throws SQLException {
        Connection conn= SqlConn.getConnection();
        String sql = "Delete from flight where id_flight=? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,id);
        ps.executeUpdate();
    }
    public  void updateFlight(Flight flight) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "Update flight set dateFrom=?,dateTo=?,airport_from=?,airport_to=?,price=?,total_seats=? where id_flight=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        settAllButNoId(flight,ps);
        ps.setInt(7,flight.getId());
        ps.executeUpdate();

    }

    private  Flight getFlightFromData(ResultSet rs) throws SQLException {
        int id = rs.getInt("id_flight");
        Date dateFrom = rs.getDate("dateFrom");
        Date dateTo = rs.getDate("dateTo");
        String airportFrom = rs.getString("airport_from");
        String airportTo = rs.getString("airport_to");
        double price = rs.getDouble("price");
        int totalSeats = rs.getInt("total_seats");
        return new Flight(id,dateFrom,dateTo,airportFrom,airportTo,price,totalSeats);
    }
    private  void settAllButNoId(Flight flight, PreparedStatement ps) throws SQLException {
        java.sql.Date dateFromm = new java.sql.Date(flight.getDepartureDate().getTime());
        java.sql.Date dateToo = new java.sql.Date(flight.getArrivalDate().getTime());
        ps.setDate(1,dateFromm);
        ps.setDate(2,dateToo);
        ps.setString(3,flight.getAirportFrom());
        ps.setString(4,flight.getAirportTo());
        ps.setDouble(5, flight.getPrice());
        ps.setInt(6,flight.getTotalSeats());

    }



}
