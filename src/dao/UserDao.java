package dao;

import model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import connection.SqlConn;

import javax.ejb.Stateless;

@Stateless
public class UserDao {


    public  List<User> getAllUsers() throws SQLException {

       List<User> users = new ArrayList<>();
       Connection conn = SqlConn.getConnection();
       String sql = "select * from user";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
           User user = getUserFromData(rs);
            users.add(user);
        }
        return users;

    }

    public  void addUser(User user) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "INSERT  INTO  user  VALUES  (?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,user.getLogin());
        ps.setString(2,user.getPassword());
        ps.setString(3,user.getName());
        ps.setString(4,user.getSurname());
        ps.setString(5,user.getCity());
        ps.setInt(6,user.getBirthdate());
        ps.executeUpdate();
    }

    public  User searchUserByLogin(String login) throws SQLException {
       Connection conn= SqlConn.getConnection();
       String sql = "Select * from user where login_user=?";
       PreparedStatement ps = conn.prepareStatement(sql);
       ps.setString(1,login);
       ResultSet rs = ps.executeQuery();
       if (rs.next()){
           return getUserFromData(rs);
       }else return null;

    }

    public  void deleteUser(String login) throws SQLException {
        Connection conn= SqlConn.getConnection();
        String sql = "Delete from user where login_user=? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,login);
        ps.executeUpdate();

    }
    public  void updateUser(User user) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "Update user set password_user=?,name_user=?,surname_user=?,city_user=?,birthyear_user=? where login_user=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(6,user.getLogin());
        ps.setString(1,user.getPassword());
        ps.setString(2,user.getName());
        ps.setString(3,user.getSurname());
        ps.setString(4,user.getCity());
        ps.setString(5, user.getBirthdate()+"");
        ps.executeUpdate();

    }


    private  User getUserFromData(ResultSet rs) throws SQLException {
        String login = rs.getString("login_user");
        String password = rs.getString("password_user");
        String name = rs.getString("name_user");
        String surname = rs.getString("surname_user");
        String city = rs.getString("city_user");
        int birthyear = rs.getInt("birthyear_user");
        return new User(login,password,name,surname,city,birthyear+"");
    }
    public  boolean isLoginValid(String login, String password) throws SQLException {
        Connection conn = SqlConn.getConnection();
        assert conn != null;
        PreparedStatement ps = conn.prepareStatement("SELECT * from user WHERE login_user=? AND password_user=?");
        if(ps==null){
            return false;
        }
        ps.setString(1,login);
        ps.setString(2,password);
        ResultSet rs = ps.executeQuery();
        if (rs.next()){
            return true;
        }else {
            return false;
        }

    }


}
