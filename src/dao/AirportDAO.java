package dao;

import connection.SqlConn;
import model.Airport;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class AirportDAO {


    public  List<Airport> getAllAirports() throws SQLException {
        List<Airport> airports = new ArrayList<>();
        Connection conn = SqlConn.getConnection();
        String sql = "select * from airport";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            String code = rs.getString("code_airport");
            String city =rs.getString("city_airport");
            String country = rs.getString("country_airport");
            airports.add(new Airport(code,city,country));

        }
        return airports;
    }

    public  void addAirPort(Airport airport) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "INSERT  INTO  airport  VALUES  (?,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,airport.getCode());
        ps.setString(2,airport.getCity());
        ps.setString(3,airport.getCountry());
        ps.executeUpdate();

    }

    public  Airport searchAirPortByCode(String code) throws SQLException {
        Connection conn= SqlConn.getConnection();
        String sql = "Select * from airport where code_airport=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,code);
        ResultSet rs = ps.executeQuery();
        if (rs.next()){
            String city =rs.getString("city_airport");
            String country = rs.getString("country_airport");
            return new Airport(code,city,country);
        }else return null;
    }

    public  void deleteAirPort(String code) throws SQLException {
        Connection conn= SqlConn.getConnection();
        String sql = "Delete from airport where code_airport=? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,code);
        ps.executeUpdate();
    }
    public  void updateAirport(Airport airport) throws SQLException {
        Connection conn = SqlConn.getConnection();
        String sql = "Update airport set city_airport=?,country_airport=? where code_airport=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(3,airport.getCode());
        ps.setString(1,airport.getCity());
        ps.setString(2,airport.getCountry());
        ps.executeUpdate();
    }

}
