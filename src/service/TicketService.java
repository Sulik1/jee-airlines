package service;

import dao.TicketDAO;
import model.Ticket;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.SQLException;
import java.util.List;

@Stateless
public class TicketService {
    @EJB
    TicketDAO ticketDAO;

    public void removeTicket(Integer id) throws SQLException {
        if (id!=null) {
            if (!Integer.toString(id).equals("")) {
                ticketDAO.deleteTicket(id);
            }
        }
    }
    public void buyTicket(String flightID, String userLogin){
        boolean alreadyBought= false;
        try {
            for (Ticket t : ticketDAO.allTicketForUser(userLogin)){
                if (t.getFlightId()==Integer.parseInt(flightID)){
                    alreadyBought=true;
                }
            }

            try {
                if (!alreadyBought) {
                    ticketDAO.addTicket(new Ticket(0, Integer.parseInt(flightID), userLogin));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addTicket(String id, String flightId, String userLog, boolean exist){
        if (id!=null&&flightId!=null&&userLog!=null){
            if (!id.equals("")||!flightId.equals("")||!userLog.equals("")){
                try {
                    for (Ticket t : ticketDAO.getAllTickets()){
                        if (t.getId()==Integer.parseInt(id)){
                            exist=true;
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (!exist){
                    try {
                        ticketDAO.addTicket(new Ticket(Integer.parseInt(id),Integer.parseInt(flightId),userLog));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }
            }

        }
    }
    public List<Ticket> allTickets() throws SQLException {
        return ticketDAO.getAllTickets();
    }
    public int howManyTicketForFlight(int idFlight) throws SQLException {
        return ticketDAO.howManyTicketOnThisFlight(idFlight);

    }
    public List<Ticket> ticketForUser(String login) throws SQLException {
       return ticketDAO.allTicketForUser(login);
    }




}
