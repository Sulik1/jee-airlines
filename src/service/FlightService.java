package service;

import dao.FlightDAO;
import model.Flight;
import model.Ticket;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Stateless
public class FlightService {
    @EJB
    FlightDAO flightDAO;

    public void removeFlight(Integer id) throws SQLException {
        if (id !=null) {
            if (!Integer.toString(id).equals("")) {
                flightDAO.deleteFlight(id);

            }
        }
    }

    public void editFlight(String id,String aDAte, String dDAte, String airFrom, String airTo, String price, String seats, String type){
        double priceNumber = Double.parseDouble(price);
        int seatsNumber = Integer.parseInt(seats);

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        try {
            Date arrivalDateAsDate = dateFormatter.parse(aDAte);
            Date departureDateAsDate = dateFormatter.parse(dDAte);

            Flight flight = new Flight(Integer.parseInt(id), departureDateAsDate, arrivalDateAsDate, airFrom, airTo, priceNumber, seatsNumber);

            if (type.equals("update")) {
                flightDAO.updateFlight(flight);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    public List<Flight> allFlights() throws SQLException {
        return flightDAO.getAllFlights();
    }
    public void addFlight(String dateFrom , String dateTo,String airportFrom, String airportTo,String price, String totalSeats){
        if ( dateFrom !=null&& dateTo !=null&& airportFrom !=null&& airportTo !=null&& price !=null&&totalSeats!=null) {
            if ( !dateFrom.equals("")&&!dateTo.equals("")&& !airportFrom.equals("")&&! airportTo.equals("")&&! price.equals("")&&!totalSeats.equals("")){

                    try {

                        try {
                            flightDAO.addFlight(new Flight(0,new SimpleDateFormat("yyyy-MM-dd").parse(dateFrom),new SimpleDateFormat("yyyy-MM-dd").parse(dateTo),airportFrom,airportTo,Double.parseDouble(price),Integer.parseInt(totalSeats)));
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

        }
    }
    public Flight searchFlight(int id) throws SQLException {
        for (Flight f : flightDAO.getAllFlights()){
            if (f.getId()==id){
                return f;
            }
        }
        return null;
    }

}
