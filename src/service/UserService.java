package service;

import dao.UserDao;
import model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserService {
    @EJB
    UserDao userDao;

    public void removeUser(String login) throws SQLException {
        if (login != null) {
            if (!login.equals("")) {
                userDao.deleteUser(login);
            }
        }
    }
    public void addUser(String login ,String password, String name, String surname, String city, String date, boolean exist) throws SQLException {
        if (login!=null&&password!=null&&name!=null&&surname!=null&&city!=null&&date!=null) {
            if (!login.equals("")&&!password.equals("")&&!name.equals("")&&!surname.equals("")&&!city.equals("")&&!date.equals("")){
                for (User u : userDao.getAllUsers()){
                    if (u.getLogin().equals(login)){
                        exist=true;
                    }
                }
                if (!exist) {
                    userDao.addUser(new User(login,password,name,surname,city,date));

                }

            }
        }
    }
    public List<User> allUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        users = userDao.getAllUsers();
        return users;
    }
    public boolean loginValidator(String login, String password){

        if (login != null && password != null) {
            try {
                if (userDao.isLoginValid(login,password)) {
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
        return false;

    }
    public void editUSer(String password,String name, String surname, String loginEdit, String city, String date, boolean exist) throws SQLException {
        for (User u : userDao.getAllUsers()){
            if (u.getLogin().equals(loginEdit)){
                exist=true;
            }
        }
        if (exist){
            if (loginEdit!=null){
                if (!loginEdit.equals("")){
                    if (password!=null&&name!= null&&surname!=null&&city!=null&&date!=null){
                        if (password.equals("")){
                            User user = userDao.searchUserByLogin(loginEdit);
                            password = user.getPassword();
                        }
                        if (name.equals("")){
                            User user = userDao.searchUserByLogin(loginEdit);
                            name = user.getName();
                        }
                        if (surname.equals("")){
                            User user = userDao.searchUserByLogin(loginEdit);
                            surname = user.getSurname();
                        }
                        if (city.equals("")){
                            User user = userDao.searchUserByLogin(loginEdit);
                            city = user.getCity();
                        }
                        if (date.equals("")){
                            User user =userDao.searchUserByLogin(loginEdit);
                            date = user.getBirthdate().toString();
                        }
                        userDao.updateUser(new User(loginEdit,password,name,surname,city,date));
                }
                }

            }

        }
    }
}
