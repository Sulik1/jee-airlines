package service;

import dao.AirportDAO;
import model.Airport;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.SQLException;
import java.util.List;

@Stateless
public class AirportService {
    @EJB
    AirportDAO airportDAO;

    public void removeAirport(String code) throws SQLException {
        if (code != null) {
            if (!code.equals("")) {
                airportDAO.deleteAirPort(code);

            }
        }
    }

    public void addAirport(String code, String city, String country) {
        if (code != null && city != null && country != null) {
            if (!code.equals("") && !city.equals("") && !country.equals("")) {
                boolean exist = false;
                try {
                    for (Airport t : airportDAO.getAllAirports()) {
                        if (t.getCode().equals(code)) {
                            exist = true;
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (!exist) {
                    try {
                        airportDAO.addAirPort(new Airport(code, city, country));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    public void updateAirport(String code, String city, String country, boolean exists) throws SQLException {
        for (Airport a : airportDAO.getAllAirports()){
            if (a.getCode().equals(code)){
                exists=true;
            }
        }

        if (exists) {
            if (code != null) {
                if (!code.equals("")) {
                    if (city != null && country != null) {
                        if (city.equals("")) {
                            Airport a = null;
                            try {
                                a = airportDAO.searchAirPortByCode(code);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            city = a.getCity();
                        }
                        if (country.equals("")) {
                            Airport a = null;
                            try {
                                a = airportDAO.searchAirPortByCode(code);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            country = a.getCountry();
                        }
                        try {
                            airportDAO.updateAirport(new Airport(code, city, country));
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        }
    }
    public List<Airport> allAirports() throws SQLException {
        return airportDAO.getAllAirports();
    }
    public Airport searchAirport(String airPort) throws SQLException {
       return airportDAO.searchAirPortByCode(airPort);
    }


}
