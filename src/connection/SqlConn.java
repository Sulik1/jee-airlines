package connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqlConn {
    public static Connection getConnection(){
        try {
            String url = "jdbc:mysql://localhost:3306/airport?autoReconnect=true&useSSL=false&serverTimezone=UTC";
            String username = "root";
            String password = "root";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            return  DriverManager.getConnection(url, username, password);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
