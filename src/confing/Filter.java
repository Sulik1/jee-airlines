package confing;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class Filter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession session = req.getSession();
        String login = (String) session.getAttribute("login");
        boolean canLog =login!=null;
        if (canLog||req.getRequestURI().endsWith("login.jsp")||req.getRequestURI().endsWith("register.jsp")||req.getRequestURI().endsWith("script")||req.getRequestURI().endsWith("css")||req.getRequestURI().endsWith("addUser")||req.getRequestURI().endsWith("login")||req.getRequestURI().endsWith("register")){
            chain.doFilter(req,res);
        }else {
            res.sendRedirect("login");
        }
    }
}
