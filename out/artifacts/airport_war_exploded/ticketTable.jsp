<%@ page import="model.Ticket" %>

<%@ page import="java.util.List" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agency - Start Bootstrap Theme</title>

    <%@ include file="css-imports.jsp"%>

</head>

<body id="page-top">
<%@ include file="header.jsp"%>


<section>
    <div class="container align-content-center text-center" id="tic">
        <h1>Tickets</h1>

        <table class="table table-responsive table-striped centered ml-auto mr-auto">
            <tr>
                <th>Id</th>
                <th>Flight Id</th>
                <th>User Login</th>
            </tr>

            <%
                List<Ticket> allTickets =(List<Ticket>) request.getAttribute("ticketList");
                for (Ticket t : allTickets) {
            %>
            <tr>
                <td><%=t.getId()%></td>
                <td><%=t.getFlightId()%></td>
                <td><%=t.getUserLogin()%></td>
                <td>
                    <a class="btn btn-danger" href="removeTicket?which=<%=t.getId()%>">
                        REMOVE
                    </a>
                </td>
            </tr>
            <%
                }

            %>
        </table>



        <div>
        <a class="btn btn-outline-success btn-xl" href="addTicket"> Add new Ticket </a>
    </div>

    </div>
</section>



</body>

</html>

