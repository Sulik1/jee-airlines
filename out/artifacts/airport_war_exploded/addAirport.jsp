
  Created by IntelliJ IDEA.
  User: DS
  Date: 27.03.2019
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="css-imports.jsp"%>
</head>
<body>
<section>
    <div class="container" >
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Add Airport</h5>
                        <form class="form-signin" method="post" action="addAirport">
                            <div class="form-label-group">
                                <input type="text" name="codeAdd" class="form-control"  placeholder="Code" required autofocus>
                                <label >Code</label>
                            </div>
                            <div class="form-label-group">
                                <input type="text" name="cityAdd" class="form-control" placeholder="City" required>
                                <label >City</label>
                            </div>
                            <div class="form-label-group">
                                <input type="text" name="countryAdd" class="form-control" placeholder="Country" required>
                                <label >Coutry</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase"  type="submit" >Add Airport</button>
                            <hr class="my-4">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<%@ include file="footer.jsp"%>

</body>
</html>

