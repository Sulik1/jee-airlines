
<%@ page import="model.User" %>
<%@ page import="java.util.List" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agency - Start Bootstrap Theme</title>

    <%@ include file="css-imports.jsp" %>

</head>

<body id="page-top">
<%@ include file="header.jsp" %>


<section>
    <div class="container align-content-center text-center">
        <h1>Users</h1>

        <table class="table table-responsive table-striped centered ml-auto mr-auto">
            <tr>
                <th>Login</th>
                <th>Name</th>
                <th>Password</th>
                <th>Surname</th>
                <th>City</th>
                <th>Birth year</th>
            </tr>

            <%
                List<User> users = (List<User>) request.getAttribute("usersList");
                for (User user : users) {
            %>
            <tr>
                <td><%=user.getLogin()%>
                </td>
                <td><%=user.getName()%>
                </td>
                <td><%=user.getPassword()%>
                </td>
                <td><%=user.getSurname()%>
                </td>
                <td><%=user.getCity()%>
                </td>
                <td><%=user.getBirthdate()%>
                </td>
                <td>
                    <div><a class="btn btn-warning" href="editUser?which=<%=user.getLogin()%>">EDIT</a></div>
                    <div><a class="btn btn-danger" href="removeUser?which=<%=user.getLogin()%>">REMOVE</a></div>
                </td>
            </tr>
            <%
                }
            %>
        </table>

        <div><a class="btn btn-outline-success btn-xl" href="addUser">Add new User</a></div>

    </div>
</section>


</body>

</html>

