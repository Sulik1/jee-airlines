<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Fru Fru Fu</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <%
                    HttpSession session11 = request.getSession();
                    if (session11.getAttribute("login").equals("admin")){
                %>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="userTable">USERS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="airportTable">AIRPORTS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="ticketTable">TICKET</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="flightTable">FLIGHTS</a>
                </li>
                <%
                    }else {
                %>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="flightTable">FLIGHTS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="allMyTickets">MY TICKETS</a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Welcome To Our Airlines!</div>
            <div class="intro-heading text-uppercase">Let's Flight</div>
        </div>
    </div>
</header>
