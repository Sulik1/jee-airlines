
<%@ page import="model.Airport" %>

<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: DS
  Date: 27.03.2019
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="css-imports.jsp"%>
</head>
<body>
<section>
    <div class="container" >
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Add Flight</h5>
                        <form class="form-signin" method="post" action="addFlight">   <div class="form-label-group">

                        </div>
                            <div class="form-label-group">
                                <input type="date" name="dateDeAdd" class="form-control" placeholder="yy-MM-dd" required>
                                <label >Departure Date</label>
                                <%
                                 List<Airport> airports = (List<Airport>) request.getAttribute("airportList");
                                %>
                            </div>
                            <div class="form-label-group">
                                <input type="date" name="dateArAdd" class="form-control" placeholder="yy-MM-dd" required>
                                <label >Arrival Date</label>
                            </div>
                            <div class="form-label-group">
                                <select type="text" name="airportFrom" class="form-control" placeholder="Airport From" required>
                                    <%
                                        for (Airport a : airports){
                                    %>
                                    <option value="<%=a.getCode()%>"><%=a.getCity()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <label >Airport From</label>
                            </div>
                            <div class="form-label-group">
                                <select type="text" name="airportTo" class="form-control" placeholder="Airport To" required>
                                    <%
                                        for (Airport a : airports){
                                    %>
                                    <option value="<%=a.getCode()%>"><%=a.getCity()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <label >Airport To</label>
                            </div>
                            <div class="form-label-group">
                                <input type="number" name="price" class="form-control" placeholder="Price" required>
                                <label >Price</label>
                            </div>
                            <div class="form-label-group">
                                <input type="number" name="totalSe" class="form-control" placeholder="Total Seats" required>
                                <label >Total Seats</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" >Add Flight</button>
                            <hr class="my-4">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<%@ include file="footer.jsp"%>

</body>
</html>