
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="css-imports.jsp"%>
</head>
<body>
<section>
    <div class="container" >
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Add User</h5>
                        <form class="form-signin" method="post" action="addUser">   <div class="form-label-group">
                            <input type="text" name="loginRe" class="form-control"  placeholder="Login" required autofocus>
                            <label >Login</label>
                        </div>
                            <div class="form-label-group">
                                <input type="password" name="passwordRe" class="form-control" placeholder="Password" required>
                                <label >Password</label>
                            </div>
                            <div class="form-label-group">
                                <input type="text" name="nameRe" class="form-control" placeholder="Name" required>
                                <label >Name</label>
                            </div>
                            <div class="form-label-group">
                                <input type="text" name="surnameRe" class="form-control" placeholder="Surname" required>
                                <label >Surname</label>
                            </div>
                            <div class="form-label-group">
                                <input type="text" name="cityRe" class="form-control" placeholder="City" required>
                                <label >City</label>
                            </div>
                            <div class="form-label-group">
                                <input type="number" name="dateRe" class="form-control" placeholder="Year" required>
                                <label >Date</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" >Add User</button>
                            <hr class="my-4">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<%@ include file="footer.jsp"%>

</body>
</html>

