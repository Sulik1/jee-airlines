<%@ page import="model.Airport" %>

<%@ page import="java.util.List" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agency - Start Bootstrap Theme</title>

    <%@ include file="css-imports.jsp" %>

</head>

<body id="page-top">
<%@ include file="header.jsp"%>


<section>
    <div class="container align-content-center text-center">
        <h1>Airports</h1>

        <table class="table table-responsive table-striped centered ml-auto mr-auto">
            <tr>
                <th>Code</th>
                <th>City</th>
                <th>Country</th>
            </tr>

            <%
                List<Airport> allAirPorts = (List<Airport>) request.getAttribute("airportTable");
                for (Airport a : allAirPorts) {
            %>
            <tr>
                <td><%=a.getCode()%>
                </td>
                <td><%=a.getCity()%>
                </td>
                <td><%=a.getCountry()%>
                </td>
                <td>
                    <div><a class="btn btn-warning" href="editAirport?which=<%=a.getCode()%>">EDIT </a></div>
                    <div><a class="btn btn-danger"href="removeAirport?which=<%=a.getCode()%>">REMOVE</a></div>
                </td>
            </tr>
            <%
                }
            %>
        </table>


        <div>
            <a class="btn btn-outline-success btn-xl" href="addAirport">Add new Airport </a>
        </div>

    </div>
</section>


</body>

</html>

