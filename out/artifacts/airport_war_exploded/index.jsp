<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Agency - Start Bootstrap Theme</title>

  <%@ include file="css-imports.jsp"%>

</head>

<body id="page-top">

<%@ include file="header.jsp"%>

<section>
  <div class="container">
    <%
      HttpSession session1 =request.getSession();
      if (session1.getAttribute("login").equals("admin")){
    %>
    <div style="text-align: center">
    <div style="margin: 50px"><a class="btn btn-outline-info btn-xl" href="userTable">USERS</a></div>
    <div style="margin: 50px"><a class="btn btn-outline-info btn-xl" href="airportTable">AIRPORTS</a></div>
    <div style="margin: 50px"><a class="btn btn-outline-info btn-xl" href="ticketTable">TICKETS</a></div>
    <div style="margin: 50px"><a class="btn btn-outline-info btn-xl" href="flightTable">FLIGHTS</a></div>
    <%@ include file="logOutButton.jsp"%>
  </div>

    <%
      }else{
    %>
      <div style="text-align: center">
    <div><a class="btn btn-outline-info btn-xl" href="flightTable">FLIGHTS</a></div>
    <%@ include file="logOutButton.jsp"%>
      </div>

    <%
      }
    %>






  </div>
</section>


<%@ include file="footer.jsp"%>

</body>

</html>
