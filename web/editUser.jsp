<%@ page import="model.User" %>

  Created by IntelliJ IDEA.
  User: DS
  Date: 01.04.2019
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="css-imports.jsp"%>
</head>
<body>
<%
    String loginEdit =(String) request.getAttribute("which");
%>
<form class="form-signin" method="post" action="editUser?which=<%=loginEdit%>" style="width: 40%;margin: auto">
    <h1>EDIT User</h1>
    <div class="form-label-group">
        <input type="password" name="passwordEdit" class="form-control" placeholder="Password">
        <label>New Password</label>
    </div>
    <div class="form-label-group">
        <input type="text" name="nameEdit" class="form-control" placeholder="Name">
        <label>New Name</label>
    </div>
    <div class="form-label-group">
        <input type="text" name="surnameEdit" class="form-control" placeholder="Surname">
        <label>New Surname</label>
    </div>
    <div class="form-label-group">
        <input type="text" name="cityEdit" class="form-control" placeholder="City">
        <label>New City</label>
    </div>
    <div class="form-label-group">
        <input type="number" name="dateEdit" class="form-control" placeholder="Year">
        <label>New BirthYear</label>
    </div>


    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">EDIT</button>
</form>



</body>
</html>
