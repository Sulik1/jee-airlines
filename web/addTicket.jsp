
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="css-imports.jsp"%>
</head>
<body>
<section>
    <div class="container" >
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Add Ticket</h5>
                        <form class="form-signin" method="post" action="addTicket">
                            <div class="form-label-group">
                                <input type="number" name="flightIdAdd" class="form-control" placeholder="Flight Id" required>
                                <label >Flight Id</label>
                            </div>
                            <div class="form-label-group">
                                <input type="text" name="userLoginAdd" class="form-control" placeholder="User Login" required>
                                <label >User Login</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase"  type="submit" >Add Ticket</button>
                            <hr class="my-4">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<%@ include file="footer.jsp"%>

</body>
</html>
