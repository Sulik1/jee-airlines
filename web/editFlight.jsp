
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="model.Flight" %>


<%@ page import="model.Airport" %>

<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agency - Start Bootstrap Theme</title>

    <%@ include file="css-imports.jsp" %>

</head>

<body id="page-top">

<section>
    <div class="container align-content-center text-center">

        <h1>Update flight</h1>

        <%
            String id = (String) request.getAttribute("which");
            Flight flight = (Flight) request.getAttribute("flight") ;
            List<Airport> airports = (List<Airport>) request.getAttribute("airportList");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        %>

        <form action="editFlight?id=<%=id%>" method="post">
            <input type="hidden" name="type" value="update">
            <div class="form-group">
                <label for="arrDate">Arrival date</label>
                <input type="datetime-local" class="form-control" id="arrDate" placeholder="Enter arrival date in format 1111-22-33T44:55" name="arrivalDate" required value="<%=dateFormatter.format(flight.getArrivalDate())%>">
            </div>

            <div class="form-group">
                <label for="depDate">Departure date</label>
                <input type="datetime-local" class="form-control" id="depDate" placeholder="Enter departure date in format 1111-22-33T44:55" name="departureDate" required value="<%=dateFormatter.format(flight.getDepartureDate())%>">
            </div>


            <div class="form-group">
                <label for="airportFrom">Airport from</label>
                <select type="text" class="form-control" id="airportFrom" placeholder="Enter airport from code..." name="airportFrom" required value="<%=flight.getAirportFrom()%>">
                    <%
                        for (Airport a : airports){
                    %>
                    <option value="<%=a.getCode()%>"><%=a.getCity()%></option>
                    <%


                        }
                    %>
                </select>
            </div>
            <div class="form-group">
                <label for="airportTo">Airport to</label>
                <select type="text" class="form-control" id="airportTo" placeholder="Enter airport to code..." name="airportTo" required value="<%=flight.getAirportTo()%>">
                    <%
                        for (Airport a : airports){
                    %>
                    <option value="<%=a.getCode()%>"><%=a.getCity()%></option>
                    <%
                        }
                    %>
                </select>
            </div>
            <div class="form-group">
                <label for="priceInput">Price</label>
                <input type="number" class="form-control" id="priceInput" placeholder="Enter price..." name="price" required value="<%=flight.getPrice()%>">
            </div>

            <div class="form-group">
                <label for="seatsInput">Seats</label>
                <input type="number" class="form-control" id="seatsInput" placeholder="Enter seats number..." name="seats" required value="<%=flight.getTotalSeats()%>">
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</section>

</body>

</html>

