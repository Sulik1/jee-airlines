<%@ page import="model.Airport" %>

<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: DS
  Date: 01.04.2019
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="css-imports.jsp"%>
</head>
<body>
<%
    String code = (String)request.getAttribute("which");
%>
<form class="form-signin" method="post" action="editAirport?which=<%=code%>" style="width: 40%;margin: auto">
    <h1>EDIT Airport</h1>
    <div class="form-label-group">
        <input type="text" name="cityEdit" class="form-control" placeholder="City">
        <label>New City</label>
    </div>
    <div class="form-label-group">
        <input type="text" name="countryEdit" class="form-control" placeholder="Country">
        <label>New Country</label>
    </div>
    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">EDIT</button>
</form>


</body>
</html>
