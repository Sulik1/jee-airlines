<%@ page import="java.util.List" %>
<%@ page import="model.Ticket" %>

<%@ page import="model.Flight" %>

<%@ page import="model.Airport" %>

<%@ page import="service.AirportService" %>
<%@ page import="service.FlightService" %>
<%@ page import="service.TicketService" %><%--
  Created by IntelliJ IDEA.
  User: DS
  Date: 07.04.2019
  Time: 13:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@ include file="logOutButton.jsp"%>
<div class="container align-content-center text-center">
    <h1>MY TICKETS</h1>
    <%

    %>

    <table class="table table-responsive table-striped centered ml-auto mr-auto text-center"style="margin: auto">
        <tr>
            <th>Departure Date</th>
            <th>Arrival Date</th>
            <th>Airport From</th>
            <th>Airport To</th>
        </tr>

        <%

            List<Ticket> tickets =(List<Ticket>) request.getAttribute("listTickets") ;
            List<Airport> airports = (List<Airport>) request.getAttribute("listAirports");
            List<Flight> flights = (List<Flight>) request.getAttribute("listFlights");
            Flight f = null;
            Airport aFrom = null;
            Airport aTo = null;
            if (!tickets.isEmpty()){
            for (Ticket t :tickets){
             for (Flight fi : flights){
                 if (t.getFlightId()==fi.getId()){
                     f=fi;
                 }
             }
             for (Airport a : airports){
                 if (a.getCode().equals(f.getAirportFrom())){
                     aFrom=a;
                 }
             }
             for (Airport a : airports){
                 if (a.getCode().equals(f.getAirportTo())){
                     aTo=a;
                 }
             }
        %>
        <tr>
            <td><%=f.getDepartureDate()%>
            </td>
            <td><%=f.getArrivalDate()%>
            </td>
            <td><%=f.getAirportFrom()%>(<%=aFrom.getCity()%>)
            </td>
            <td><%=f.getAirportTo()%> (<%=aTo.getCity()%>)
            </td>
            <%

                }
                }else out.write("lista jest nullem");
            %>
        </tr>
    </table>

</div>
</body>
</html>
