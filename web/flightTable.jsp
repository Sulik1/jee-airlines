<%@ page import="model.Flight" %>

<%@ page import="java.util.List" %>

<%@ page import="model.Airport" %>
<%@ page import="service.TicketService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agency - Start Bootstrap Theme</title>

    <%@ include file="css-imports.jsp"%>

</head>

<body id="page-top">
<%@ include file="header.jsp"%>


<section>
    <div class="container align-content-center text-center">
        <h1>Flights</h1>

        <table class="table table-responsive table-striped centered ml-auto mr-auto">
            <%
                List<Flight> allflights = (List<Flight>) request.getAttribute("flightTable");
                String flightId = request.getParameter("bought");
                String user =(String) request.getAttribute("user");
                if (user!=null){
            %>
            <tr>
                <th>Id</th>
                <th>Departure Date</th>
                <th>Arrival Date</th>
                <th>Airport From</th>
                <th>Airport To</th>
                <th>Price</th>
                <th>Total Seats</th>
            </tr>

            <%

                List<Airport> airports = (List<Airport>)request.getAttribute("airportTable");
                Airport aFrom =null;
                Airport aTo=null;

                for (Flight f : allflights) {
                    for (Airport a :airports){
                        if (f.getAirportFrom().equals(a.getCode())){
                          aFrom=a;
                        }
                        if (f.getAirportTo().equals(a.getCode())){
                            aTo=a;
                        }
                    }

            %>
            <tr>
                <td><%=f.getId()%></td>
                <td><%=f.getDepartureDate()%></td>
                <td><%=f.getArrivalDate()%></td>
                <td><%=f.getAirportFrom()%> (<%=aFrom.getCity()%>)</td>
                <td><%=f.getAirportTo()%> (<%=aTo.getCity()%>)</td>
                <td><%=f.getPrice()%></td>
                <td><%=f.getTotalSeats()%></td>
                <td>
                    <div><a class="btn btn-warning" href="editFlight?id=<%=f.getId()%>">EDIT</a></div>
                    <div><a class="btn btn-danger" href="removeFlight?which=<%=f.getId()%>">REMOVE</a></div>
                </td>
            </tr>


<%
                    }

            %>


        </table>
    </div>
    <div style="text-align: center"><a class="btn btn-outline-success btn-xl" href="addFlight">Add new Flight</a></div>
</section>
<%
    }else {


%>

<tr>
    <th>Id</th>
    <th>Departure Date</th>
    <th>Arrival Date</th>
    <th>Airport From</th>
    <th>Airport To</th>
    <th>Price</th>
    <th>Total Seats</th>

</tr>
<%
    TicketService ticketService = new TicketService();

    for (Flight f :allflights){


%>
<tr>
    <td><%=f.getId()%>
    </td>
    <td><%=f.getDepartureDate()%>
    </td>
    <td><%=f.getArrivalDate()%>
    </td>
    <td><%=f.getAirportFrom()%>
    </td>
    <td><%=f.getAirportTo()%>
    </td>
    <td><%=f.getPrice()%>
    </td>
    <td><%=f.getTotalSeats()%>
    </td>
    <td>

        <%
            if (flightId == null) {
        %>
        <div><a class="btn btn-success" href="buyTicket?id=<%=f.getId()%>">BUY</a></div>
        <%
        } else if (Integer.parseInt(flightId) == f.getId()) {
        %>
        <div><a class="btn btn-danger">BOUGHT</a></div>
        <%
        } else {
        %>
        <div><a class="btn btn-success" href="buyTicket?id=<%=f.getId()%>">BUY</a></div>
        <%
                }
            }
        %>


    </td>
</tr>
<%

    }
%>


</div>
</section>



</body>

</html>

